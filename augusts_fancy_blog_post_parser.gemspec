Gem::Specification.new do |s|
  s.name = "augusts_fancy_blog_post_parser"
  s.version = "0.1.0"
  s.authors = ["August Lilleaas"]
  s.email = "august@augustl.com"
  s.summary = "August's fancy blog post parser"
  s.add_runtime_dependency "nokogiri", "~> 1.5.0"
  s.add_runtime_dependency "parallel", "~> 0.7.0"
  s.files = [
    "lib/augusts_fancy_blog_post_parser.rb",
    "augusts_fancy_blog_post_parser.gemspec"
  ]
end
