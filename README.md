# August's Fancy Blog Post Parser

This is the fancy blog post parser I use at augustl.com. Example:

https://github.com/augustl/augustl.com/blob/a84f09f9a5a14df71cc9c082c7e7412b5de8a9fe/posts/blog/2013/using_zeromq_from_clojure.html

MIT Licensed.

Copyright 2013 August Lilleaas
